<?php

/**
 *  @module         ckeditor
 *  @version        see info.php of this module
 *  @authors        Dietrich Roland Pehlke, erpe
 *  @copyright      2012-2024 Dietrich Roland Pehlke, erpe
 *  @license        CKEditor 5 license
 *  @license terms  see info.php of this module
 */


class ckeditor extends LEPTON_abstract
{
    public array $toolbars = [];
	public array $skins = [];
    
    public static $instance;

    protected bool $debug = false;

    public function initialize()
    {
        $this->toolbars = ckeditor_settings::getInstance()->toolbars;
		$this->skins = ckeditor_settings::getInstance()->skins;
    }

    /**
     *    returns the template name of the current displayed page
     * 
     *    @param string  $css_path  A path to the editor.css - if there is one. Default is an empty string. Pass by reference!
     *    @return string $tiny_template_dir
     */
    static function get_template_name( &$css_path = "")
    {
        $database = LEPTON_database::getInstance();
        
        $lookup_paths = array(
            '/css/editor.css',
            '/editor.css'
        );

        $cke_template_dir = "none";

        /**
         *    Looking up for an editor.css file
         *
         */
        foreach($lookup_paths as $temp_path) {
            if (file_exists(LEPTON_PATH .'/templates/' .DEFAULT_TEMPLATE .$temp_path ) ) {
                $css_path = $temp_path; // keep in mind, that this one is pass_by_reference
                $cke_template_dir = DEFAULT_TEMPLATE;
                break;
            }
        }

        // check if an editor.css file exists in the specified template directory of current page
        if (isset($_GET["page_id"]) && ((int) $_GET["page_id"] > 0))
        {
            $pageid = intval( $_GET["page_id"] );
            // obtain template folder of current page from the database
            $query_page = "SELECT `template` FROM `" .TABLE_PREFIX ."pages` WHERE `page_id`='".$pageid."'";
            $pagetpl = $database->get_one($query_page);

            /**
             *    check if a specific template is defined for current page
             *
             */
            if (isset($pagetpl) && ($pagetpl != '')) {    
                /**
                 *    check if a specify editor.css file is contained in that folder
                 *
                 */
                foreach($lookup_paths as $temp_path) {
                    if (file_exists(LEPTON_PATH.'/templates/'.$pagetpl.$temp_path)) {
                        $css_path = $temp_path; // keep in mind, that this one is pass_by_reference
                        $cke_template_dir = $pagetpl;
                        break;
                    }
                }
            }
        }
        return $cke_template_dir;
    } // get_template_name()

    /**
     * Initialize CKE and create an textarea
     * 
     * @param string $name     Name of the textarea.
     * @param string $id       Id of the textarea.
     * @param string $content  The content to edit.
     * @param int|string|null $width    The width of the editor, overwritten by wysiwyg-admin.
     * @param int|string|null $height   The height of the editor, overwritten by wysiwyg-admin.
     * @param bool $prompt  Direct output to the client via echo (true) or returnd as HTML-textarea (false)?
     * @return bool|string        Could be a BOOL or STR (textarea-tags).
     *
     */
    static function show_wysiwyg_editor(
        string $name,
        string $id,
        string $content,
        int|string|null $width=NULL,
        int|string|null $height=NULL,
        bool $prompt=true
    ): bool|string
    {
        // global $id_list;

        $sHTMLScriptPart = "";

        $database = LEPTON_database::getInstance();

        /**
         *  [0.1] Get Twig
         */
        $oTWIG = lib_twig_box::getInstance();

        //  [0.1.1] Prepend path to make sure twig is looking in this module template folder first
        $oTWIG->loader->prependPath(dirname(__DIR__)."/templates/");

        /** *****
         *  1. CKE main script part
         *
         */

        /**
         * Since L* 5.5: wysiwyg_settings
         */
        if (class_exists("wysiwyg_settings", true))
        {
            $oEditorSettings = wysiwyg_settings::getInstance();
        } else {
            $custom = (class_exists("ckeditor_settings_custom", true)) ?"_custom" : "";
            $oEditorSettings = eval("return ckeditor_settings".$custom."::getInstance();");
        }

        $cke_url = LEPTON_URL."/modules/ckeditor/ckeditor";

        $temp_css_path = "/editor.css";
        $template_name = self::get_template_name( $temp_css_path );

        /**
         *    If editor.css file exists in default template folder or template folder of current page
         *  See: http://www.tinymce.com/wiki.php/Configuration:content_css
         */
        $css_file = '"'.LEPTON_URL.'/templates/'.$template_name.$temp_css_path.'"';

        if (!file_exists (LEPTON_PATH.'/templates/'.$template_name.$temp_css_path)) 
        {
            $css_file = "''";
        }

        /**
         *    If backend.css file exists in default theme folder overwrite module backend.css
         */
        $backend_css = LEPTON_URL.'/templates/'.DEFAULT_THEME.'/backend/ckeditor/backend.css';
        if(!file_exists(LEPTON_PATH.'/templates/'.DEFAULT_THEME.'/backend/ckeditor/backend.css')) {
            $backend_css = LEPTON_URL.'/modules/ckeditor/css/backend.css';
        }

        /**
         *    Include language file
         *    If the file is not found (local) we use an empty string,
         *    TinyMCE will use english as the defaut language in this case.
         */
        $lang = strtolower(LANGUAGE);
        $language = (file_exists(dirname(__DIR__)."/ckeditor/translations/". $lang .".js" )) ? $lang : "";

        
        // define filemanager url and access keys
        $filemanager_url = LEPTON_URL."/modules/lib_r_filemanager";
        $akey = password_hash(LEPTON_GUID, PASSWORD_DEFAULT);
        $akey = str_replace(['$','/'],'',$akey);
        $akey = substr($akey, -30);
        $_SESSION['rfkey'] = $akey;

        if (!defined("CKE_LOADED"))
        {
            define("CKE_LOADED", true);
            $cke_loaded = false;
            
        } else {
            $cke_loaded = CKE_LOADED;
        }
        $data = array(
            'cke_loaded'         => $cke_loaded,
            'filemanager_url'   => $filemanager_url,
            'LEPTON_URL'        => LEPTON_URL,
            'ACCESS_KEY'        => $akey,            
            'cke_url'           => $cke_url,
            'backend_css'       => $backend_css,            
            'selector'          => '#'.$id,//'textarea:not(#no_wysiwyg)',
            'language'          => $language,      
            'debug'             => self::getInstance()->debug,
            'width'             => $width ?? $oEditorSettings->getWidth(),
            'height'            => $height ?? $oEditorSettings->getHeight(),
            'toolbar'           => ($oEditorSettings->editor_settings['toolbar']=="Full" ? "" : json_encode( $oEditorSettings->getToolbar() ) ),
            'skin'              => json_encode( $oEditorSettings->getSkin() ),
            'css_file'          => $css_file,
            'id'                => $id,

        );

        $sHTMLScriptPart = $oTWIG->render( 
            "ckeditor.lte",
            $data
        );

        /** *****
         *  2. textarea part
         *
         */

        //  values for the textarea
        $data = [
            'id'        => $id,
            'name'      => $name,
            'content'   => htmlspecialchars_decode( $content ),
            'width'     => $oEditorSettings->getWidth(),
            'height'    => $oEditorSettings->getHeight()
        ];

        $result = $oTWIG->render(
            'textarea.lte',
            $data
        );

        $result .= $sHTMLScriptPart;

        if ($prompt)
        {
            echo $result;
            return true;
        }
        return $result;

    }

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     */
    public function setDebug(bool $debug): void
    {
        $this->debug = $debug;
    }
}
