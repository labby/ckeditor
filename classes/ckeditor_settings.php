<?php

/**
 *  @module         ckeditor
 *  @version        see info.php of this module
 *  @authors        Dietrich Roland Pehlke, erpe
 *  @copyright      2012-2024 Dietrich Roland Pehlke, erpe
 *  @license        CKEditor 5 license
 *  @license terms  see info.php of this module
 *
 */
 
class ckeditor_settings extends LEPTON_abstract implements LEPTON_wysiwyg_interface
{
    public $default_skin    = "moono-lisa";
    public $default_height  = "250"; // in px
    public $default_width   = "100"; // in %
    public $default_toolbar = "Simple";
    public $default_content_css = "document";

    public $toolbar = "Simple";

    public $skins       = [];
    public $content_css = [];
    public $toolbars    = [];

    public $content     = "Hello LEPTON-CMS - this is the CKEditor 5 here. <a href='https://ckeditor.com/docs/index.html'>Documentation</a>";

    public static $instance;

    public function initialize() 
    {
//      $ignore_files = array("index.php");
//
//      $temp_skins  = array_slice(scanDir(LEPTON_PATH.'/modules/tinymce/tinymce/skins/ui/'), 2);
//      $this->skins = array_diff($temp_skins, $ignore_files);
//
//      $temp_content_css = array_slice(scanDir(LEPTON_PATH.'/modules/tinymce/tinymce/skins/content/'), 2);
//      $this->content_css = array_diff($temp_content_css, $ignore_files);
//
            $this->toolbars = [

                'Full'    => [
                    'heading', '|',
                    'fontfamily', 'fontsize', '|',
                    'alignment', '|',
                    'fontColor', 'fontBackgroundColor', '|',
                    'bold', 'italic', 'strikethrough', 'underline', 'subscript', 'superscript', '|',
                    'link',
                    '-',
                    'outdent', 'indent', '|',
                    'bulletedList', 'numberedList', 'todoList', '|',
                    'code', 'codeBlock', '|',
                    'insertTable', '|',
                    'image', 'uploadImage', 'blockQuote', '|',
                    'undo', 'redo'
                ],

                'Smart' => [
                    'italic', 'bold', '|', 'undo', 'redo', '|', 'bulletedList', 'numberedList', '|', 'blockQuote' , 'link'
                ],

                'Simple' => [
                    'heading', '|','bold','italic','|','link','|','bulletedList','numberedList','uploadImage','blockQuote','undo','redo'
                ],

                /**
                 *    This one is for experimental use only. Use this one for your own studies and
                 *    development e.g. own plugins, icons, tools, etc.
                 *
                 */
                'Experimental' => [
                    'Source', '-', 'Italic', 'Bold', 'Underline', 'Strike', '-', 'Undo', 'Redo' ,
                    'Image', 'HorizontalRule', 'SpecialChar' ,
                    '/',
                    'Droplets', 'Pagelink', '-', 'Unlink', 'Anchor', 'Link' ,
                    'Shybutton', 'TextColor',
                    'About' 
                ]
            ];
    }
    
    // interfaces
    //  [1]
    public function getHeight()
    {
       return $this->default_height; 
    }
    //  [2]
    public function getWidth()
    {
        return $this->default_width; 
    }
    //  [3]
    public function getToolbar()
    {
        return $this->toolbars[ $this->default_toolbar ]; 
    }
    //  [4]
    public function getSkin()
    {
        return $this->default_skin ?? ""; 
    }
}

