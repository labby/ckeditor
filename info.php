<?php

/**
 *  @module         ckeditor
 *  @version        see info.php of this module
 *  @authors        Dietrich Roland Pehlke, erpe
 *  @copyright      2012-2024 Dietrich Roland Pehlke, erpe
 *  @license        CKEditor 5 license
 *  @license terms  see info.php of this module
 */

// Download: https://ckeditor.com/ckeditor-5/download/

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$module_directory   = 'ckeditor';
$module_name        = 'CKEditor';
$module_function    = 'WYSIWYG';
$module_version     = '5.41.4.2'; // build-classic-41.4.2
$module_platform    = '7.x';
$module_author      = 'erpe, Dietrich Roland Pehlke (Aldus)';
$module_license     = '<a target="_blank" href="https://ckeditor.com/wysiwyg-editor-open-source/">CKEditor 5 license</a>';
$module_license_terms = '-';
$module_description = 'Includes latest (see release notice) CKEditor; CKE allows editing content and can be integrated in frontend and backend modules.';
$module_guid        = '76c55e3d-3ba9-4222-9db8-5aa2ba5b52ba';
$module_home        = 'https://cms-lab.com';
