### CKEditor
============

This project is the implementation of the CK-Editor 5 to LEPTON CMS.

#### Requirements

* [LEPTON CMS][1], current Version


#### Installation

* download latest [.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"
* for customising and configuration see [CKEditor][3]

#### Notice

After installing addon you are done. <br />
Create a new page with this addon.

#### Changelog

see commits on github at https://gitlab.com/labby/ckeditor/commits/master

[1]: https://lepton-cms.org "LEPTON CMS"
[2]: https://lepton-cms.com/lepador/editors/ck-editor-5series.php
[3]: https://ckeditor.com/docs/ckeditor5/latest/builds/guides/overview.html#classic-editor "CKEditor configuration"
