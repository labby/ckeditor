<?php

/**
 *  @module         ckeditor
 *  @version        see info.php of this module
 *  @authors        Dietrich Roland Pehlke, erpe
 *  @copyright      2012-2024 Dietrich Roland Pehlke, erpe
 *  @license        CKEditor 5 license
 *  @license terms  see info.php of this module
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php



// delete unneeded files
$directories = array(
    '/modules/ckeditor/config',
	'/modules/ckeditor/ckeditor/adapters',
    '/modules/ckeditor/ckeditor/lang',
    '/modules/ckeditor/ckeditor/plugins',
    '/modules/ckeditor/ckeditor/skins',
	'/modules/ckeditor/ckeditor/vendor'
);
LEPTON_handle::delete_obsolete_directories($directories);

$files = array(
    '/modules/ckeditor/ckeditor/translations/it.js',
	'/modules/ckeditor/ckeditor/translations/fi.js',
	'/modules/ckeditor/ckeditor/translations/pl.js',
	'/modules/ckeditor/ckeditor/translations/ru.js',
	'/modules/ckeditor/class.editorinfo.php',
    '/modules/ckeditor/CHANGELOG.md',
	'/modules/ckeditor/ckeditor/build-config.js',
	'/modules/ckeditor/ckeditor/CHANGES.md',
    '/modules/ckeditor/ckeditor/config.js',
    '/modules/ckeditor/ckeditor/contents.css',
    '/modules/ckeditor/ckeditor/styles.js'
);
LEPTON_handle::delete_obsolete_files ($files); 